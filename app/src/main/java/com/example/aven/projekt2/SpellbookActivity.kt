package com.example.aven.projekt2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.support.v7.widget.Toolbar
import kotlinx.android.synthetic.main.activity_spellbook.*

class SpellbookActivity() : AppCompatActivity(), AddingDialog.OnAddingDialogInputListener {
    var currentSpellbook: Spellbook? = null

    var adapter: SpellListAdapter? = null

    val db = DatabaseManager(this)

    var mToolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spellbook)
        val extras = intent.extras ?: return
        var id = extras.getInt("spellbookId")

        try{
            db.createDataBase()
        }catch (e: Exception){
            e.printStackTrace()
        }
        try{
            db.openDataBase()
        }catch (e: Exception){
            e.printStackTrace()
        }
        mToolbar = findViewById<View>(R.id.toolbar) as Toolbar

        mToolbar?.setNavigationOnClickListener {
            finish()
        }

        currentSpellbook = DatabaseManager(this).loadSpellbook(id)

        adapter = SpellListAdapter(currentSpellbook!!.listOfNeededSpells, this)

        spellbookElementsList.adapter=adapter

        spellbookElementsList.setOnItemLongClickListener { adapterView, view, i, l ->
            currentSpellbook!!.removeSpellAt(i)
            adapter?.notifyDataSetChanged()
            true
        }

        fabExport.setOnClickListener{
            var dialog = AddingDialog()
            dialog.adapter = MiniSpellListAdapter(db.getAllSpells(), this)
            dialog.show(fragmentManager, "AddingDialog")
        }
    }

    override fun onInput(nameInput: String) {
        currentSpellbook!!.addSpell(db.fetchSpell(nameInput.toInt())?: Spell())
        adapter!!.notifyDataSetChanged()
    }

    override fun finish() {
        db.saveSpellbook(currentSpellbook!!)
        super.finish()
    }

    override fun onPause() {
        db.saveSpellbook(currentSpellbook!!)
        super.onPause()
    }

    override fun onDestroy() {
        db.saveSpellbook(currentSpellbook!!)
        super.onDestroy()
    }

}
