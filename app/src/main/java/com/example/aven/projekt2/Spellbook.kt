package com.example.aven.projekt2

import android.databinding.ObservableArrayList

/**
 * Created by Aven on 2018-05-21.
 */
class Spellbook(n: String, id: Int) {
    var listOfNeededSpells: ObservableArrayList<Spell> = ObservableArrayList()
    val name: String = n
    val spellbookId: Int = id

    fun addSpell(b: Spell){
        listOfNeededSpells.add(b)
    }

    fun removeSpellAt(i: Int){
        listOfNeededSpells.removeAt(i)
    }
}