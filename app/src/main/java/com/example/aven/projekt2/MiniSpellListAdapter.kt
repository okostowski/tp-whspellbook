package com.example.aven.projekt2

import android.content.Context
import android.databinding.ObservableArrayList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*


/**
 * Created by Aven on 2018-05-21.
 */
class MiniSpellListAdapter: ArrayAdapter<MiniSpell> {

    private val dataSet: ObservableArrayList<MiniSpell>
    var mContext: Context

    // View lookup cache
    private class ViewHolder {
        internal var txtName: TextView? = null
        internal var txtId: TextView? = null
    }

    constructor(data: ObservableArrayList<MiniSpell>, context: Context):super(context, R.layout.minispell_layout, data) {
        this.dataSet = data
        this.mContext = context
    }

    private var lastPosition = -1

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        // Get the data item for this position
        val dataModel = getItem(position)
        // Check if an existing view is being reused, otherwise inflate the view
        val viewHolder: ViewHolder // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = ViewHolder()
            val inflater = LayoutInflater.from(context)
            convertView = inflater.inflate(R.layout.minispell_layout, parent, false)
            viewHolder.txtName = convertView!!.findViewById(R.id.minispellName)
            viewHolder.txtId = convertView!!.findViewById(R.id.minispellId)
            convertView!!.setTag(viewHolder)
        } else {
            viewHolder = convertView!!.tag as ViewHolder
        }

        lastPosition = position

        viewHolder.txtName!!.setText("Nazwa: ${dataModel!!.nazwa}")
        viewHolder.txtId!!.setText("ID: ${dataModel!!.id}")


        // Return the completed view to render on screen
        return convertView
    }

    fun getMinispellId(pos: Int): Int{
        return dataSet.get(pos).id
    }

}