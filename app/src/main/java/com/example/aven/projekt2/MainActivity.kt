package com.example.aven.projekt2

import android.content.Intent
import android.databinding.ObservableArrayList
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.os.StrictMode
import android.util.Log
import com.example.aven.projekt2.NewSpellbookDialog.OnDialogInputListener


class MainActivity : AppCompatActivity(), OnDialogInputListener {

    override fun onInput(nameInput: String) {
        var newSpellbookId = db.addSpellbook(nameInput)
        openSpellbook(newSpellbookId)
    }

    var spellbookList: ObservableArrayList<Spellbook> = ObservableArrayList<Spellbook>()
    var adapter: SpellbookListAdapter? = null
    val db: DatabaseManager = DatabaseManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
        try{
            db.createDataBase()
        }catch (e: Exception){
            e.printStackTrace()
        }
        try{
            db.openDataBase()
        }catch (e: Exception){
            e.printStackTrace()
        }

        loadSpellbooks()

        adapter = SpellbookListAdapter(spellbookList, this)

        listView.adapter=adapter

        listView.setOnItemClickListener { parent, view, i, l ->
            var item = listView.getItemAtPosition(i) as Spellbook
            openSpellbook(item.spellbookId)
        }

        fab.setOnClickListener{
            var dialog = NewSpellbookDialog()
            dialog.show(fragmentManager, "CreationDialog")
        }
    }

    override fun onResume() {
        loadSpellbooks()
        super.onResume()
    }

    override fun onRestart() {
        loadSpellbooks()
        super.onRestart()
    }


    fun loadSpellbooks(){
        var tempList = db.fetchSpellbooks()
        Log.e("PROJECT NO", "${tempList.count()}")
        spellbookList.clear()
        spellbookList.addAll(tempList)
    }

    fun openSpellbook(id: Int){
        var intent = Intent(this, SpellbookActivity::class.java)
        intent.putExtra("spellbookId", id)
        startActivity(intent)
    }

}
