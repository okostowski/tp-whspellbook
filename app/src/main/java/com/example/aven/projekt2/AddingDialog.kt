package com.example.aven.projekt2

import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView

/**
 * Created by Aven on 2018-05-29.
 */
class AddingDialog: DialogFragment() {
    val TAG: String = "DialogExportFragment"

    var adapter: MiniSpellListAdapter? = null

    interface OnAddingDialogInputListener{
        fun onInput(nameInput: String)
    }
    var listener: OnAddingDialogInputListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater!!.inflate(R.layout.dialog_adding_layout, container, false)

        view.findViewById<ListView>(R.id.allSpellsList).adapter = adapter
        view.findViewById<ListView>(R.id.allSpellsList).setOnItemClickListener { adapterView, view, i, l ->
            listener?.onInput(adapter!!.getMinispellId(i).toString())

            dialog.dismiss()
        }

        view.findViewById<Button>(R.id.buttonNo).setOnClickListener {
            dialog.dismiss()
        }

//        view.findViewById<Button>(R.id.buttonYes).setOnClickListener {
//            var name = nameEdit.text.toString()
//            listener?.onInput(name)
//
//            dialog.dismiss()
//        }

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try{
            listener = activity as OnAddingDialogInputListener
        }catch (e: ClassCastException){
            e.printStackTrace()
        }
    }
}